﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Employees.Data.EF;
using System.Linq.Expressions;

namespace Employees.Data.EF
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private IRepository<TEntity> _repository;

        public EFRepository(EFUnitOfWork uow)
        {
            _repository = new GenericRepository<TEntity, EmployeesContext>(uow.Context);
        }

        public TEntity Read(int id)
        {
            return _repository.Read(id);
        }

        public IQueryable<TEntity> All()
        {
            return _repository.All();
        }

        public IQueryable<TEntity> AllIncluding(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _repository.AllIncluding(includeProperties);
        }

        public void Create(TEntity entity)
        {
            _repository.Create(entity);
        }

        public void Update(TEntity entity)
        {
            _repository.Update(entity);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }
    }
}
