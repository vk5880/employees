﻿
namespace Employees.Data.EF.Configurations
{
    using Employees.Models;
    using System.Data.Entity.ModelConfiguration;

    class ProgrammingLangueageConfiguration : EntityTypeConfiguration<ProgrammingLanguage>
    {
        public ProgrammingLangueageConfiguration()
        {
            Property(e => e.Name).HasMaxLength(64).IsRequired();
        }
    }
}
