﻿
namespace Employees.Data.EF.Configurations
{
    using Employees.Models;
    using System.Data.Entity.ModelConfiguration;

    class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            Property(e => e.FirstName).HasMaxLength(64).IsRequired();
            Property(e => e.LastName).HasMaxLength(64).IsRequired();
            Property(e => e.Age).IsOptional();
            Property(e => e.SexId).IsOptional().HasColumnName("Sex");
            Ignore(e => e.Sex);
            HasRequired(e => e.Department).WithMany().HasForeignKey(e => e.DepartmentId);
            HasRequired(e => e.ProgrammingLanguage).WithMany().HasForeignKey(e => e.ProgrammingLanguageId);
        }
    }
}
