﻿
namespace Employees.Data.EF.Configurations
{
    using Employees.Models;
    using System.Data.Entity.ModelConfiguration;

    class DepartmentConfiguration : EntityTypeConfiguration<Department>
    {
        public DepartmentConfiguration()
        {
            Property(e => e.Name).HasMaxLength(128).IsRequired();
            Property(e => e.Floor).IsOptional();
        }
    }
}
