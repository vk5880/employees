﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using Employees.Models;

namespace Employees.Data.EF
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private EmployeesContext _context;

        public EFUnitOfWork()
        {
            _context = new EmployeesContext();
        }

        internal EmployeesContext Context { get { return _context; } }

        public void Save()
        {
            _context.SaveChanges();
        }

        private IRepository<Employee> _employeeRepository;
        public IRepository<Employee> Employees
        {
            get 
            {
                return _employeeRepository ?? (_employeeRepository = new GenericRepository<Employee, EmployeesContext>(_context));
            }
        }

        private IRepository<Department> _departmentRepository;
        public IRepository<Department> Departments
        {
            get
            {
                return _departmentRepository ?? (_departmentRepository = new GenericRepository<Department, EmployeesContext>(_context));
            }
        }

        private IRepository<ProgrammingLanguage> _programmingLanguageRepository;
        public IRepository<ProgrammingLanguage> ProgrammingLanguages
        {
            get
            {
                return _programmingLanguageRepository ?? (_programmingLanguageRepository = new GenericRepository<ProgrammingLanguage, EmployeesContext>(_context));
            }
        }

        private bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_context != null) _context.Dispose();
                }
                _context = null;
                _disposed = true;
            }
        }
    }
}
