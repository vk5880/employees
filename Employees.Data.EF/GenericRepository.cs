﻿namespace Employees.Data.EF
{
    using System.Linq;
    using System.Data;
    using System.Data.Entity;
    using System.Linq.Expressions;

    class GenericRepository<TEntity, TContext> : IRepository<TEntity> where TEntity: class where TContext: DbContext
    {
        private TContext _context;

        public GenericRepository(TContext context)
        {
            _context = context;
        }

        public TEntity Read(int id)
        {
            var entity = _context.Set<TEntity>().Find(id);

            return entity;
        }

        public IQueryable<TEntity> All()
        {
            var query = _context.Set<TEntity>();

            return query;
        }

        public IQueryable<TEntity> AllIncluding(params Expression<System.Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> query = _context.Set<TEntity>();
            foreach (var property in includeProperties)
            {
                query = query.Include(property);
            }

            return query;
        }

        public void Create(TEntity entity)
        {
            var entry = _context.Entry<TEntity>(entity);
            entry.State = EntityState.Added;
        }

        public void Update(TEntity entity)
        {
            var entry = _context.Entry<TEntity>(entity);
            entry.State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var entity = _context.Set<TEntity>().Find(id);
            _context.Set<TEntity>().Remove(entity);
        }
    }
}
