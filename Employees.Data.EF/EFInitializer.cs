﻿namespace Employees.Data.EF
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using Employees.Models;

    class EFInitializer : DropCreateDatabaseIfModelChanges<EmployeesContext>
    {
        protected override void Seed(EmployeesContext context)
        {
            SeedEmployees(context);

            base.Seed(context);
        }

        private void SeedEmployees(EmployeesContext context)
        {
            context.Set<Employee>().Add(new Employee
                {
                    FirstName = "Владислав",
                    LastName = "Костенко",
                    Age = 28,
                    Sex = Sex.Male,
                    Department = new Department { Name = "Department1", Floor = 1 },
                    ProgrammingLanguage = new ProgrammingLanguage { Name = "C#" }
                }
            );
            context.Set<Employee>().Add(new Employee
                {
                    FirstName = "Иван",
                    LastName = "Иванов",
                    Age = 27,
                    Sex = Sex.Male,
                    Department = new Department { Name = "Department2", Floor = 2 },
                    ProgrammingLanguage = new ProgrammingLanguage { Name = "C++" }
                }
            );
            context.Set<Employee>().Add(new Employee
                {
                    FirstName = "Ольга",
                    LastName = "Петрова",
                    Age = 29,
                    Sex = Sex.Female,
                    Department = new Department { Name = "Department3", Floor = 3 },
                    ProgrammingLanguage = new ProgrammingLanguage { Name = "Java" }
                }
            );
        }
    }
}
