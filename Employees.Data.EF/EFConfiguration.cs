﻿
namespace Employees.Data.EF
{
    using System.Data.Entity;

    public static class EFConfiguration
    {
        public static void Configure()
        {
            var initializer = new EFInitializer();
            Database.SetInitializer(initializer);
            using(var context = new EmployeesContext())
            {
                initializer.InitializeDatabase(context);
            }
        }
    }
}
