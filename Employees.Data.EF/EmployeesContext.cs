﻿namespace Employees.Data.EF
{
    using System.Data.Entity;
    using Employees.Models;
    using Employees.Data.EF.Configurations;

    class EmployeesContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<ProgrammingLanguage> ProgrammingLanguages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations
                .Add(new DepartmentConfiguration())
                .Add(new EmployeeConfiguration())
                .Add(new ProgrammingLangueageConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
