﻿/// <reference path="../jquery-1.10.2.js" />
$(function () {
    var cache = {};
    $("#firstName").autocomplete({
        source: function (request, response) {
            var query = request.term;
            if (query in cache) {
                response(cache[query]);
                return;
            }
            $.post("Employee/GetAutocompleteNames", { query: query }, function (data) {
                if (data instanceof Array) {
                    cache[query] = data;
                    response(data);
                }
                else {
                    response([]);
                }
            }, "json");
        }
    });

    $("select").chosen();
});