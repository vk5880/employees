﻿/// <reference path="../jquery-1.10.2.js" />
$(function () {
    var deleteEmployee = function (e) {
        e.preventDefault();
        var employeeId = $(e.target).data("id");
        $("#deleteEmployeeId").val(employeeId);
        var form = $("#deleteEmployeeForm");
        form.submit();
    };

    $(".deleteEmployeeLink").click(deleteEmployee);
});