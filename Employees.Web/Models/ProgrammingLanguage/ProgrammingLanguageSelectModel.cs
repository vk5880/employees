﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Employees.Web.Models.ProgrammingLanguage
{
    public class ProgrammingLanguageSelectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}