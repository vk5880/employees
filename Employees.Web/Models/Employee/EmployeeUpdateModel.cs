﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Employees.Models;
using System.ComponentModel.DataAnnotations;

namespace Employees.Web.Models.Employee
{
    public class EmployeeUpdateModel : EmployeeCreateModel
    {
        [Required]
        public int Id { get; set; }
    }
}