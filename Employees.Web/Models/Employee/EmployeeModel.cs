﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Employees.Web.Models.Employee
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public string ProgrammingLanguage { get; set; }
    }
}