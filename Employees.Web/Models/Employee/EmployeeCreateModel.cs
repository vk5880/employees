﻿using System.Collections.Generic;
using Employees.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Employees.Web.Models.Department;
using Employees.Web.Models.ProgrammingLanguage;

namespace Employees.Web.Models.Employee
{
    public class EmployeeCreateModel
    {
        [DisplayName("Имя")]
        [Required]
        public string FirstName { get; set; }
        
        [DisplayName("Фамилия")]
        [Required]
        public string LastName { get; set; }
        
        [DisplayName("Возраст")]
        [Range(1, 128)]
        public int Age { get; set; }
        
        [DisplayName("Пол")]
        public Sex Sex { get; set; }

        [DisplayName("Отдел")]
        [Required]
        public int DepartmentId { get; set; }

        public ICollection<DepartmentSelectModel> Departments { get; set; }

        [DisplayName("Язык программирования")]
        [Required]
        public int ProgrammingLanguageId { get; set; }

        public ICollection<ProgrammingLanguageSelectModel> ProgrammingLanguages { get; set; }
    }
}