﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Employees.Web.Models.Department
{
    public class DepartmentSelectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}