﻿namespace Employees.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper;
    using Employees.Models;
    using Employees.Data;
    using Employees.Web.Models.Employee;
    using Employees.Web.Models.Department;
    using Employees.Web.Models.ProgrammingLanguage;
    using Employees.Data.EF;

    public class EmployeeController : Controller
    {
        IUnitOfWork _uow;

        public EmployeeController()
        {
            // it is good idea to use dependency injection here
            _uow = new EFUnitOfWork();
        }

        public ActionResult Index()
        {
            var employees = _uow.Employees
                .AllIncluding(e => e.Department, e => e.ProgrammingLanguage)
                .OrderBy(e => e.Id)
                .ToArray();
            var model = employees.Select(Mapper.Map<EmployeeModel>).ToArray();

            return View(model);
        }

        public ActionResult Create()
        {
            var model = new EmployeeCreateModel();
            SetDepartmentsAndProgrammingLanguages(model);
            // initialization of a new employee with the default values
            model.Age = 18;
            model.Sex = Sex.Male;

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Create(EmployeeCreateModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var employee = Mapper.Map<Employee>(model);
            _uow.Employees.Create(employee);
            _uow.Save();

            return RedirectToAction("Index");
        }

        public ActionResult Update(int id)
        {
            if (id <= 0)
            {
                return new HttpNotFoundResult();
            }

            var employee = _uow.Employees.Read(id);
            var model = Mapper.Map<EmployeeUpdateModel>(employee);
            SetDepartmentsAndProgrammingLanguages(model);

            return View(model);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Update(EmployeeUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var employee = Mapper.Map<Employee>(model);
            _uow.Employees.Update(employee);
            _uow.Save();

            return RedirectToAction("Index");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            if (id <= 0) 
            {
                return HttpNotFound();
            }

            _uow.Employees.Delete(id);
            _uow.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult GetAutocompleteNames(string query)
        {
            if (String.IsNullOrEmpty(query))
            {
                return new JsonResult();
            }

            var employees = _uow.Employees
                .All()
                .Select(e => e.FirstName)
                .Distinct()
                .Where(e => e.Contains(query))
                .Take(10)
                .ToArray();

            return Json(employees);
        }

        private void SetDepartmentsAndProgrammingLanguages(EmployeeCreateModel model)
        {
            var departments = _uow.Departments.All().ToArray() ;
            var departmentsModels = departments.Select(Mapper.Map<DepartmentSelectModel>).ToArray();
            var programmingLanguages = _uow.ProgrammingLanguages.All().ToArray();
            var programmingLanguagesModels = programmingLanguages.Select(Mapper.Map<ProgrammingLanguageSelectModel>).ToArray();

            model.Departments = departmentsModels;
            model.ProgrammingLanguages = programmingLanguagesModels;
        }

        private bool _disposed;
        protected override void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_uow != null) 
                    { 
                        _uow.Dispose(); 
                    } 

                    base.Dispose(disposing);

                    _disposed = true;
                }
            }
        }
    }
}