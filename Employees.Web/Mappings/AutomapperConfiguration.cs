﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using AutoMapper;

namespace Employees.Web.Mappings
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            var executingAssembly = Assembly.GetExecutingAssembly();
            var configurations = executingAssembly.GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && typeof(IMappingConfiguration).IsAssignableFrom(t));
            foreach (Type configurationType in configurations) 
            {
                var configuration = (IMappingConfiguration)Activator.CreateInstance(configurationType);
                configuration.Configure();
            }
        }
    }
}