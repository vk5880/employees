﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Employees.Web.Mappings.Employee
{
    using Employees.Models;
    using Employees.Web.Models.Department;

    public class DepartmentSelectModelMappingConfiguration : IMappingConfiguration
    {
        public void Configure()
        {
            Mapper.CreateMap<Department, DepartmentSelectModel>();
        }
    }
}