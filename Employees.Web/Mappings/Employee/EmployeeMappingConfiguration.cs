﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Employees.Web.Mappings.Employee
{
    using Employees.Models;
    using Employees.Web.Models.Employee;

    public class EmployeeMappingConfiguration : IMappingConfiguration
    {
        public void Configure()
        {
            Mapper.CreateMap<Employee, EmployeeModel>()
                .ForMember(m => m.Department, c => c.MapFrom(e => e.Department.Name))
                .ForMember(m => m.ProgrammingLanguage, c => c.MapFrom(e => e.ProgrammingLanguage.Name));
            Mapper.CreateMap<Employee, EmployeeCreateModel>();
            Mapper.CreateMap<EmployeeCreateModel, Employee>();
            Mapper.CreateMap<Employee, EmployeeUpdateModel>();
            Mapper.CreateMap<EmployeeUpdateModel, Employee>();
        }
    }
}