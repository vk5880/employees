﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Employees.Web.Mappings
{
    interface IMappingConfiguration
    {
        void Configure();
    }
}