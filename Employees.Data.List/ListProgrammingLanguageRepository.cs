﻿using Employees.Models;
using System.Collections.Generic;

namespace Employees.Data.List
{
    public class ListProgrammingLanguageRepository : ListRepository<ProgrammingLanguage>
    {
        static IList<ProgrammingLanguage> _programmingLanguages;
        static ListProgrammingLanguageRepository()
        {
            _programmingLanguages = new List<ProgrammingLanguage> {
                new ProgrammingLanguage { Id = NewId, Name = "C#" },
                new ProgrammingLanguage { Id = NewId, Name = "C++" },
                new ProgrammingLanguage { Id = NewId, Name = "Java" }
            };
        }

        public ListProgrammingLanguageRepository()
        {
            _entities = _programmingLanguages;
        }
    }
}
