﻿using System.Collections.Generic;
using System.Linq;
using Employees.Models;
using System.Linq.Expressions;
using System;

namespace Employees.Data.List
{
    public class ListRepository<T> : IRepository<T> where T : IEntity
    {
        protected static int _newid = 0;
        protected static int NewId
        {
            get
            {
                return ++_newid;
            }
        }

        protected IList<T> _entities = new List<T>();

        public virtual T Read(int id)
        {
            var entity = _entities.SingleOrDefault(e => e.Id == id);

            return entity;
        }

        public virtual IQueryable<T> All()
        {
            return _entities.AsQueryable();
        }

        public virtual IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            return All();
        }

        public virtual void Create(T entity)
        {
            entity.Id = NewId;
            _entities.Add(entity);
        }

        public virtual void Update(T entity)
        {
            Delete(entity.Id);
            _entities.Add(entity);
        }

        public virtual void Delete(int id)
        {
            var entity = _entities.Single(e => e.Id == id);
            _entities.Remove(entity);
        }
    }
}
