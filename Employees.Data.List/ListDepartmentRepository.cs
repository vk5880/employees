﻿using Employees.Models;
using System.Collections.Generic;

namespace Employees.Data.List
{
    public class ListDepartmentRepository : ListRepository<Department>
    {
        static IList<Department> _departments;
        static ListDepartmentRepository()
        {
            _departments = new List<Department>() {
                new Department { Id = NewId, Name = "Department1", Floor = 1 },
                new Department { Id = NewId, Name = "Department2", Floor = 2 },
                new Department { Id = NewId, Name = "Department3", Floor = 3 }
            };
        }

        public ListDepartmentRepository()
        {
            _entities = _departments;
        }
    }
}
