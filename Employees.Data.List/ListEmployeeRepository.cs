﻿using System;
using System.Collections.Generic;
using System.Linq;
using Employees.Models;

namespace Employees.Data.List
{
    public class ListEmployeeRepository : ListRepository<Employee>
    {
        static IList<Employee> _employees;
        static IRepository<Department> _departments = new ListDepartmentRepository();
        static IRepository<ProgrammingLanguage> _programmingLanguages = new ListProgrammingLanguageRepository();
        static ListEmployeeRepository()
        {
            _employees = new List<Employee>
            {
                new Employee
                {
                    Id = NewId,
                    FirstName = "Владислав",
                    LastName = "Костенко",
                    Age = 28,
                    Sex = Sex.Male,
                    DepartmentId = 1,
                    Department = _departments.Read(1),
                    ProgrammingLanguageId = 1,
                    ProgrammingLanguage = _programmingLanguages.Read(1)
                },
                new Employee
                {
                    Id = NewId,
                    FirstName = "Иван",
                    LastName = "Иванов",
                    Age = 27,
                    Sex = Sex.Female,
                    DepartmentId = 2,
                    Department = _departments.Read(2),
                    ProgrammingLanguageId = 2,
                    ProgrammingLanguage = _programmingLanguages.Read(2)
                },
                new Employee
                {
                    Id = NewId,
                    FirstName = "Ольга",
                    LastName = "Петрова",
                    Age = 29,
                    Sex = Sex.Female,
                    DepartmentId = 3,
                    Department = _departments.Read(3),
                    ProgrammingLanguageId = 3,
                    ProgrammingLanguage = _programmingLanguages.Read(3)
                }
            };
        }

        public ListEmployeeRepository()
        {
            this._entities = _employees;
        }

        public override void Create(Employee entity)
        {
            SyncReferences(entity);
            base.Create(entity);
        }

        public override void Update(Employee entity)
        {
            SyncReferences(entity);
            base.Update(entity);
        }

        private void SyncReferences(Employee employee)
        {
            if (employee.Department != null)
            {
                employee.DepartmentId = employee.Department.Id;
            }
            else if (employee.DepartmentId > 0)
            {
                employee.Department = _departments.Read(employee.DepartmentId);
            }
            if (employee.ProgrammingLanguage != null)
            {
                employee.ProgrammingLanguageId = employee.ProgrammingLanguage.Id;
            }
            else if (employee.ProgrammingLanguageId > 0)
            {
                employee.ProgrammingLanguage = _programmingLanguages.Read(employee.ProgrammingLanguageId);
            }
        }
    }
}
