﻿namespace Employees.Data
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IRepository<T>
    {
        T Read(int id);
        IQueryable<T> All();
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);

        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
    }
}
