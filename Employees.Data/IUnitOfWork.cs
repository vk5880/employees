﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Employees.Models;

namespace Employees.Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();

        IRepository<Employee> Employees { get; }
        IRepository<Department> Departments { get; }
        IRepository<ProgrammingLanguage> ProgrammingLanguages { get; }
    }
}
