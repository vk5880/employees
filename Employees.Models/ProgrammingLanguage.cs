﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Employees.Models
{
    public class ProgrammingLanguage : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
