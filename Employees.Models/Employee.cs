﻿namespace Employees.Models
{
    public class Employee : IEntity
    {
        public int Id { get; set; }
        public string FirstName {get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Sex Sex { get; set;}
        public int SexId
        {
            get
            { 
                return (int)Sex; 
            }
            set
            {
                Sex = (Sex)value;
            }
        }
        public int DepartmentId { get; set; }
        public virtual Department Department { get; set; }
        public int ProgrammingLanguageId { get; set; }
        public virtual ProgrammingLanguage ProgrammingLanguage { get; set; }
    }
}